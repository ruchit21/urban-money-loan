import { Button, TextField } from '@mui/material'
import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom';
import { toast } from 'react-toastify';
import getAPIProgressData from '../apiHelper';
import authActions from '../redux/reducers/auth/action';
import { useDispatch } from "react-redux";

const initialState = {
    email: "",
    password: ""
}

const { setAccessToken } = authActions;

const Login = () => {

    const [state, setState] = useState(initialState);
    const { email, password } = state;
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const handleChange = (e) => {
        const { name, value } = e.target;
        setState({ ...state, [name]: value });
    }

    const handleSubmit = (e) => {
        e.preventDefault();

        const endPoint = '/user/user-form/user-login';
        const data = state;

        getAPIProgressData(endPoint, "post", data)
            .then((result) => {
                console.log("result", result);
                if (result?.success === true) {
                    const token = result?.data?.token;
                    toast.success(result?.message);
                    localStorage.setItem('token', token);
                    dispatch(setAccessToken(token));
                    navigate("/");
                } else {
                    toast.error(result?.message);
                }
            })
            .catch((err) => {
                toast.error(err);
            });
    }

    return (
        <div style={{ height: '100%', width: '100%', display: 'flex', alignItems: 'center', justifyContent: 'center' }}>
            <div style={{ width: '350px' }}>
                <TextField fullWidth className="urban_money_input" type="text" label="Email" variant="outlined" name="email" placeholder="Enter Email" onChange={handleChange} />
                <TextField fullWidth className="urban_money_input" type="password" label="Password" variant="outlined" name="password" placeholder="Enter password" onChange={handleChange} />
                <Button fullWidth className="urban_money_btn" variant="contained" onClick={handleSubmit}>Login</Button>
            </div>
        </div>
    )
}

export default Login