import { isObject, isString } from "lodash";
import { api_url } from "./api";

export default function getAPIProgressData(
    endpoint,
    method,
    data,
    headers = true,
    onProgress = null
) {
    const isOnline = window.navigator.onLine;
    if (isOnline) {
        const token = localStorage.getItem('token');
        const authheaders = {
            "Content-Type": "application/json",
            "lms-authorization": token ? 'Bearer ' + token : "",
        }
        return new Promise((resolve, reject) => {
            const url = api_url + endpoint;
            const oReq = new XMLHttpRequest();
            oReq.upload.addEventListener("progress", (event) => {
                if (event.lengthComputable) {
                    const progress = (event.loaded * 100) / event.total;
                    if (onProgress) {
                        onProgress(progress);
                    }
                }
            });

            var FormData = require("form-data");
            var form = new FormData();
            if (data && Object.keys(data).length > 0) {
                Object.keys(data).map((k) => form.append(k, data[k]));
            }

            const hData = {
                "Content-Type" : "multipart/form-data",
            };

            let options = {
                method: method,
                headers: headers ? authheaders : hData,
                body: form
            }

            delete options.headers["Content-Type"];

            fetch(url, options)
            .then(function (res) {
                const result = res.json();
                resolve(result);
            })
            .then(function (result) {
                if (result.code === 401) {
                    localStorage.clear();
                    window.location.reload();
                    resolve(result);
                }
            })
            .catch((err) => {
                console.log(err);
                reject(err);
            })
        })
    }
}

export function getAPIData(endpoint, method, data, headers) {
    const token = "" || "";
    const authheaders = {
        "Content-Type": "application/json",
        "lms-authorization": token ? 'Bearer ' + token : "",
    }

    return new Promise((resolve, reject) => {
        let query = "";
        let qs = "";

        for (const key in data) {
            query += `${encodeURIComponent(key)}=${encodeURIComponent(data[key])}&`;
        }
        const params = {};
        params.method = method.toLowerCase() === "get" ? "get" : "post";
        if (headers) {
            params.headers = headers.headers;
        } else {
            params.headers = authheaders;
        }
        if (params.method === "post") {
            if (params.headers && params.headers["Content-Type"] && params.headers["Content-Type"] === "application/json") {
                params.body = JSON.stringify(data);
            } else {
                params.body = query;
            }
        } else {
            qs = `${query}`;
        }

        if (params.method === "post" && params.headers && params.headers["Content-Type"] && params.headers["Content-Type"] === "application/json") {
            console.log(JSON.stringify(data));
        } else {
            let str = "";
            if (data && Object.keys(data).length > 0) {
                Object.keys(data).map((dk) => {
                    str += `${dk}:${data[dk]}\n`;
                });
            }
        }

        fetch(api_url + endpoint + qs, params)
            .then((response) => response.json())
            .then((responseJson) => {
                if (responseJson.code === 401) {
                    localStorage.clear();
                    window.location.reload();
                    resolve(responseJson);
                } else if (isObject(responseJson) && isString(responseJson.message) && responseJson.message === "Unauthorized" && endpoint !== "delete-token") {
                    resolve(responseJson);
                } else {
                    resolve(responseJson);
                }
            })
            .catch((err) => {
                reject(err);
            })
    })
}