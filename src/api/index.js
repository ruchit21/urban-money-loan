export const api_url = "http://localhost:4000";

export const setHeader = () => {
    const headers = {
        headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer " + localStorage.getItem("token"),
        }
    };

    return headers;
}