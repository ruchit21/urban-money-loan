import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { Route, Routes, useNavigate } from 'react-router-dom'
import Login from './pages/Login'

const App = () => {

  const auth = useSelector((state) => state.auth);
  console.log("auth", auth);
  const navigate = useNavigate();

  useEffect(() => {
    if (auth.accessToken === '') navigate("/login");
  }, [auth.accessToken, navigate])

  return (
    <div>
      <Routes>
        <Route exact path='/login' element={<Login />} />
      </Routes>
    </div>
  )
}

export default App